#include <ctype.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define M_PI 3.14159265358979323846

struct
{
    int row;
    int coloumn;
} zig_zag[8 * 8] =
{
    { 0, 0 },
    { 0, 1 },{ 1, 0 },
    { 2, 0 },{ 1, 1 },{ 0, 2 },
    { 0, 3 },{ 1, 2 },{ 2, 1 },{ 3, 0 },
    { 4, 0 },{ 3, 1 },{ 2, 2 },{ 1, 3 },{ 0, 4 },
    { 0, 5 },{ 1, 4 },{ 2, 3 },{ 3, 2 },{ 4, 1 },{ 5, 0 },
    { 6, 0 },{ 5, 1 },{ 4, 2 },{ 3, 3 },{ 2, 4 },{ 1, 5 },{ 0, 6 },
    { 0, 7 },{ 1, 6 },{ 2, 5 },{ 3, 4 },{ 4, 3 },{ 5, 2 },{ 6, 1 },{ 7, 0 },
    { 7, 1 },{ 6, 2 },{ 5, 3 },{ 4, 4 },{ 3, 5 },{ 2, 6 },{ 1, 7 },
    { 2, 7 },{ 3, 6 },{ 4, 5 },{ 5, 4 },{ 6, 3 },{ 7, 2 },
    { 7, 3 },{ 6, 4 },{ 5, 5 },{ 4, 6 },{ 3, 7 },
    { 4, 7 },{ 5, 6 },{ 6, 5 },{ 7, 4 },
    { 7, 5 },{ 6, 6 },{ 5, 7 },
    { 6, 7 },{ 7, 6 },
    { 7, 7 }
};


struct int_triplet
{
    int first;
    int second;
    int third;
};

struct double_triplet
{
    double first;
    double second;
    double third;
};

struct ppm_data
{
    char file_type[3];
    int width;
    int height;
    int max_value;
    struct int_triplet* rgb_data;
};

void free_ppm_data(struct ppm_data* data_ptr);

struct double_block
{
    struct double_triplet triplets[8][8];
};

struct ycbcr_block_data
{
    int width;
    int height;
    struct double_block* blocks;
};

struct ycbcr_block_data copy_ycbcr_block_data(struct ycbcr_block_data ycbcr);

void free_ycbcr_block_data(struct ycbcr_block_data* data_ptr);

struct ppm_data decode(char* filename);

void write_to_file(struct ppm_data* ppm, char* filename);

int main(int argc, char** argv)
{
    struct ppm_data rgb = decode("out.txt");
    write_to_file(&rgb, "out.ppm");
    free_ppm_data(&rgb);
    return 0;
}

void free_ppm_data(struct ppm_data* data_ptr)
{
    free(data_ptr->rgb_data);
    data_ptr->rgb_data = NULL;
}

struct ycbcr_block_data copy_ycbcr_block_data(struct ycbcr_block_data ycbcr)
{
    int block_count = (ycbcr.width / 8) * (ycbcr.height / 8);
    struct double_block* old_blocks = ycbcr.blocks;
    ycbcr.blocks = (struct double_block*) malloc(block_count * sizeof(struct double_block));
    memcpy(ycbcr.blocks, old_blocks, block_count * sizeof(struct double_block));
    return ycbcr;
}

void free_ycbcr_block_data(struct ycbcr_block_data* data_ptr)
{
    free(data_ptr->blocks);
}

struct ycbcr_block_data read_from_file(char* filename)
{
    FILE* file = fopen(filename, "r");
    struct ycbcr_block_data rv;

    fscanf(file, "%d %d\n\n", &rv.width, &rv.height);

    int block_count = (rv.width / 8) * (rv.height / 8);
    rv.blocks = (struct double_block*) malloc(block_count * sizeof(struct double_block));

    for(int b = 0; b < block_count; ++b)
    {
        struct double_block* block = rv.blocks + b;
        for(int i = 0; i < 64; i++)
        {
            int temp;
            fscanf(file, "%d ", &temp);
            block->triplets[zig_zag[i].row][zig_zag[i].coloumn].first = temp;
        }
    }
    for(int b = 0; b < block_count; ++b)
    {
        struct double_block* block = rv.blocks + b;
        for(int i = 0; i < 64; i++)
        {
            int temp;
            fscanf(file, "%d ", &temp);
            block->triplets[zig_zag[i].row][zig_zag[i].coloumn].second = temp;
        }
    }
    for(int b = 0; b < block_count; ++b)
    {
        struct double_block* block = rv.blocks + b;
        for(int i = 0; i < 64; i++)
        {
            int temp;
            fscanf(file, "%d ", &temp);
            block->triplets[zig_zag[i].row][zig_zag[i].coloumn].third = temp;
        }
    }

    fclose(file);

    return rv;
}

void inverse_quantisation(struct ycbcr_block_data* ycbcr)
{
    const float k1_table[8][8] =
    {
        { 16, 11, 10, 16, 24, 40, 51, 61 },
        { 12, 12, 14, 19, 26, 58, 60, 55 },
        { 14, 13, 16, 24, 40, 57, 69, 56 },
        { 14, 17, 22, 29, 51, 87, 80, 62 },
        { 18, 22, 37, 56, 68, 109, 103, 77 },
        { 24, 35, 55, 64, 81, 104, 113, 92 },
        { 49, 64, 78, 87, 103, 121, 120, 101 },
        { 72, 92, 95, 98, 112, 100, 103, 99 }
    };

    const float k2_table[8][8] =
    {
        { 17, 18, 24, 47, 99, 99, 99, 99 },
        { 18, 21, 26, 66, 99, 99, 99, 99 },
        { 24, 26, 56, 99, 99, 99, 99, 99 },
        { 47, 66, 99, 99, 99, 99, 99, 99 },
        { 99, 99, 99, 99, 99, 99, 99, 99 },
        { 99, 99, 99, 99, 99, 99, 99, 99 },
        { 99, 99, 99, 99, 99, 99, 99, 99 },
        { 99, 99, 99, 99, 99, 99, 99, 99 }
    };

    int block_count = (ycbcr->width / 8) * (ycbcr->height / 8);
    for(int b = 0; b < block_count; ++b)
    {
        struct double_block* block = ycbcr->blocks + b;
        for(int u = 0; u < 8; ++u)
        {
            for(int v = 0; v < 8; ++v)
            {
                block->triplets[u][v].first = round(block->triplets[u][v].first * k1_table[u][v]);
                block->triplets[u][v].second = round(block->triplets[u][v].second * k2_table[u][v]);
                block->triplets[u][v].third = round(block->triplets[u][v].third * k2_table[u][v]);
            }
        }
    }
}

void inverse_discrete_cosine_transform(struct ycbcr_block_data* ycbcr)
{
    struct ycbcr_block_data copy = copy_ycbcr_block_data(*ycbcr);

    int block_count = (ycbcr->width / 8) * (ycbcr->height / 8);
    for(int b = 0; b < block_count; ++b)
    {
        struct double_block* block = ycbcr->blocks + b;
        struct double_block* original_block = copy.blocks + b;
        for(int i = 0; i < 8; ++i)
        {
            for(int j = 0; j < 8; ++j)
            {
                double sum_y = 0;
                double sum_cb = 0;
                double sum_cr = 0;

                for(int v = 0; v < 8; ++v)
                {
                    for(int u = 0; u < 8; ++u)
                    {
                        double cu = (u == 0 ? 1 / sqrt(2) : 1);
                        double cv = (v == 0 ? 1 / sqrt(2) : 1);

                        double factor = cu * cv * cos((2 * i + 1) * u * M_PI / 16.0) * cos((2 * j + 1) * v * M_PI / 16.0);
                        sum_y += original_block->triplets[u][v].first * factor;
                        sum_cb += original_block->triplets[u][v].second * factor;
                        sum_cr += original_block->triplets[u][v].third * factor;
                    }

                }

                block->triplets[i][j].first = 0.25 * sum_y;
                block->triplets[i][j].second = 0.25 * sum_cb;
                block->triplets[i][j].third = 0.25 * sum_cr;
            }
        }
    }
    free_ycbcr_block_data(&copy);
}

void shift_ycbcr(struct ycbcr_block_data* ycbcr, int value)
{
    int block_count = (ycbcr->width / 8) * (ycbcr->height / 8);
    for(int b = 0; b < block_count; ++b)
    {
        struct double_block* block = ycbcr->blocks + b;
        for(int u = 0; u < 8; ++u)
        {
            for(int v = 0; v < 8; ++v)
            {
                block->triplets[u][v].first += value;
                block->triplets[u][v].second += value;
                block->triplets[u][v].third += value;
            }
        }
    }
}

struct ppm_data convert_ycbcr_to_ppm(struct ycbcr_block_data* ycbcr)
{
    struct ppm_data rv =
    {
        .file_type = "P6",
        .width = ycbcr->width,
        .height = ycbcr->height,
        .max_value = 255,
        .rgb_data = (struct int_triplet*) malloc(ycbcr->width * ycbcr->height * sizeof(struct int_triplet))
    };

    int blocks_per_row = rv.width / 8;
    int block_count = (ycbcr->width / 8) * (ycbcr->height / 8);
    struct int_triplet* rgb_data = rv.rgb_data;
    for(int b = 0; b < block_count; b += blocks_per_row)
    {
        for(int row = 0; row < 8; ++row)
        {
            for(int block = 0; block < blocks_per_row; ++block)
            {
                struct double_block* current_block = ycbcr->blocks + b + block;
                for(int column = 0; column < 8; ++column, ++rgb_data)
                {
                    struct double_triplet* current = &current_block->triplets[row][column];

                    struct int_triplet triplet = 
                    {
                        .first = max(0, min(255, current->first + 1.4 * (current->third - 128))),
                        .second = max(0, min(255, current->first -0.343 * (current->second - 128) - 0.711 * (current->third - 128))),
                        .third = max(0, min(255, current->first + 1.765 * (current->second - 128)))
                    };

                    *rgb_data = triplet;
                }
            }
        }

    }

    return rv;
}

struct ppm_data decode(char* filename)
{
    struct ycbcr_block_data ycbcr = read_from_file(filename);
    inverse_quantisation(&ycbcr);
    inverse_discrete_cosine_transform(&ycbcr);
    shift_ycbcr(&ycbcr, 128);

    struct ppm_data ppm = convert_ycbcr_to_ppm(&ycbcr);

    free_ycbcr_block_data(&ycbcr);

    return ppm;
}

void write_to_file(struct ppm_data* ppm, char* filename)
{
    FILE* file = fopen(filename, "wb");
    int length = ppm->width * ppm->height;
    fprintf(file, "P6\n%d %d\n255\n", ppm->width, ppm->height);
    for(int i = 0; i < length; ++i)
    {
        unsigned char buffer[3] = { ppm->rgb_data[i].first, ppm->rgb_data[i].second, ppm->rgb_data[i].third };
        fwrite(buffer, sizeof(unsigned char), 3, file);
    }
    fclose(file);
}
