#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef unsigned char pixel;

struct picture
{
    int width;
    int height;
    pixel* pixels;
};

struct group_count
{
    int count[16];
};

void free_picture(struct picture picture);

struct picture read_picture(char* filename);

struct group_count count_groups(struct picture picture);

void print_counts(struct group_count counts);

int main(int argc, char** argv)
{
    struct picture picture = read_picture(argv[1]);
    struct group_count count = count_groups(picture);
    print_counts(count);
    free_picture(picture);
    return 0;
}

void free_picture(struct picture picture)
{
    free(picture.pixels);
}

struct picture read_picture(char* filename)
{
    FILE* file = fopen(filename, "rb");
    struct picture rv;

    fscanf(file, "P5\n");
    fscanf(file, "%d\n", &rv.width);
    fscanf(file, "%d\n", &rv.height);
    fscanf(file, "255\n");

    int count = rv.width * rv.height;
    rv.pixels = (pixel*)malloc(sizeof(pixel) * count);
    fread(rv.pixels, sizeof(pixel), count, file);

    fclose(file);

    return rv;
}

struct group_count count_groups(struct picture picture)
{
    struct group_count rv;
    memset(rv.count, 0, sizeof(int) * 16);

    int count = picture.height * picture.width;
    for(int i = 0; i < count; ++i)
    {
        ++rv.count[picture.pixels[i] >> 4];
    }

    return rv;
}

void print_counts(struct group_count counts)
{
    for(int i = 0; i < 16; ++i)
    {
        printf("%d\t\t%d\n", counts.count[i], i);
    }
}