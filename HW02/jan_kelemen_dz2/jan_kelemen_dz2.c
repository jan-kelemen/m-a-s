#include <stdlib.h>
#include <stdio.h>

typedef unsigned char pixel;

struct picture
{
    int width;
    int height;
    pixel** pixels;
};

struct point
{
    int x;
    int y;
};

void free_picture(struct picture picture);

struct picture read_picture(char* filename);

struct point full_search(struct picture from, struct picture to, int block);

int main(int argc, char** argv)
{
    int block = atoi(argv[1]);
    char* first_filename = argc == 2 ? "lenna.pgm" : argv[2];
    char* second_filename = argc == 2 ? "lenna1.pgm" : argv[3];

    struct picture lenna = read_picture(first_filename);
    struct picture lenna1 = read_picture(second_filename);

    //for(int i = 0; i < 1024; ++i)
    //{
    //    struct point vector = full_search(lenna, lenna1, i);
    //    printf("Blok: %d - (%d, %d)\n", i, vector.x, vector.y);
    //}

    struct point vector = full_search(lenna, lenna1, block);
    printf("(%d,%d)\n", vector.x, vector.y);

    free_picture(lenna);
    free_picture(lenna1);
    return 0;
}

void free_picture(struct picture picture)
{
    for(int i = 0; i < picture.height; ++i)
    {
        free(picture.pixels[i]);
    }
    free(picture.pixels);
}

struct picture read_picture(char* filename)
{
    FILE* file = fopen(filename, "rb");
    struct picture rv;

    fscanf(file, "P5\n");
    fscanf(file, "%d\n", &rv.width);
    fscanf(file, "%d\n", &rv.height);
    fscanf(file, "255\n");

    rv.pixels = (pixel**)malloc(sizeof(pixel*) * rv.height);
    for(int i = 0; i < rv.height; ++i)
    {
        rv.pixels[i] = (pixel*)malloc(sizeof(pixel) * rv.width);
        fread(rv.pixels[i], sizeof(pixel), rv.width, file);
    }

    fclose(file);

    return rv;
}

struct point upper_right_corner_of_block(int block, int width)
{
    int blocks_per_row = width / 16;
    int vertical_blocks = block / blocks_per_row;
    int horizontal_blocks = block % blocks_per_row;

    struct point rv = { .x = horizontal_blocks * 16,.y = vertical_blocks * 16 };
    return rv;
}

struct point full_search(struct picture from, struct picture to, int block)
{
    struct point point = upper_right_corner_of_block(block, from.width);

    int block_start_x = point.x;
    int block_start_y = point.y;

    struct point vector = { .x = -16,.y = -16 };
    double mad = INT_MAX;

    for(int i = block_start_y - 16; i <= block_start_y + 16; ++i)
    {
        if(i < 0) continue;
        if(i + 15 >= from.height)
        {
            break;
        }
        for(int j = block_start_x - 16; j <= block_start_x + 16; ++j)
        {
            if(j < 0) continue;;
            if(j + 15 >= from.width) break;

            double current_mad = 0;
            for(int y = 0; y < 16; ++y)
            {
                for(int x = 0; x < 16; ++x)
                {
                    //printf("%d %d\n", i + y, j + x);
                    current_mad += (double)abs(from.pixels[i + y][j + x] - to.pixels[block_start_y + y][block_start_x + x]) / 256;
                }
            }

            //printf("\n");
            if(current_mad < mad)
            {
                mad = current_mad;
                vector.x = j - block_start_x;
                vector.y = i - block_start_y;
            }
        }
    }

    return vector;
}
