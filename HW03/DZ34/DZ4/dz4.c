#include <ctype.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include "ippi.h"

#define M_PI 3.14159265358979323846

struct
{
    int row;
    int coloumn;
} zig_zag[8 * 8] =
{
    { 0, 0 },
    { 0, 1 },{ 1, 0 },
    { 2, 0 },{ 1, 1 },{ 0, 2 },
    { 0, 3 },{ 1, 2 },{ 2, 1 },{ 3, 0 },
    { 4, 0 },{ 3, 1 },{ 2, 2 },{ 1, 3 },{ 0, 4 },
    { 0, 5 },{ 1, 4 },{ 2, 3 },{ 3, 2 },{ 4, 1 },{ 5, 0 },
    { 6, 0 },{ 5, 1 },{ 4, 2 },{ 3, 3 },{ 2, 4 },{ 1, 5 },{ 0, 6 },
    { 0, 7 },{ 1, 6 },{ 2, 5 },{ 3, 4 },{ 4, 3 },{ 5, 2 },{ 6, 1 },{ 7, 0 },
    { 7, 1 },{ 6, 2 },{ 5, 3 },{ 4, 4 },{ 3, 5 },{ 2, 6 },{ 1, 7 },
    { 2, 7 },{ 3, 6 },{ 4, 5 },{ 5, 4 },{ 6, 3 },{ 7, 2 },
    { 7, 3 },{ 6, 4 },{ 5, 5 },{ 4, 6 },{ 3, 7 },
    { 4, 7 },{ 5, 6 },{ 6, 5 },{ 7, 4 },
    { 7, 5 },{ 6, 6 },{ 5, 7 },
    { 6, 7 },{ 7, 6 },
    { 7, 7 }
};


struct int_triplet
{
    int first;
    int second;
    int third;
};

struct ppm_data
{
    char file_type[3];
    int width;
    int height;
    int max_value;
    struct int_triplet* rgb_data;
};

void free_ppm_data(struct ppm_data* data_ptr);

struct double_block
{
    float first[8][8];
    float second[8][8];
    float third[8][8];
};

struct ycbcr_block_data
{
    int width;
    int height;
    struct double_block* blocks;
};

void free_ycbcr_block_data(struct ycbcr_block_data* data_ptr);

struct ycbcr_block_data encode(char* filename);

void write_to_file(struct ycbcr_block_data* ycbcr, char* filename);

int main(int argc, char** argv)
{
    if(argc == 1)
    {
        fprintf(stderr, "filename not specified\n");
        return EXIT_FAILURE;
    }
    struct ycbcr_block_data encoded_data = encode(argv[1]);
    write_to_file(&encoded_data, "out.txt");
    free_ycbcr_block_data(&encoded_data);
    return 0;
}

void free_ppm_data(struct ppm_data* data_ptr)
{
    free(data_ptr->rgb_data);
    data_ptr->rgb_data = NULL;
}

void free_ycbcr_block_data(struct ycbcr_block_data* data_ptr)
{
    free(data_ptr->blocks);
}

struct ppm_data read_header_data(FILE* file)
{
    struct ppm_data rv;
    char buffer[1025] = { 0 };

    int header_data_read = 0;
    while(header_data_read != 3)
    {
        fgets(buffer, 1025, file);
        if(buffer[0] == '#') { continue; }

        switch(header_data_read)
        {
        case 0:
            sscanf(buffer, "%2c\n", &rv.file_type);
            rv.file_type[2] = '\0';
            break;
        case 1:
            sscanf(buffer, "%d %d\n", &rv.width, &rv.height);
            break;
        case 2:
            sscanf(buffer, "%d\n", &rv.max_value);
            break;
        }
        ++header_data_read;
    }

    return rv;
}

struct ppm_data read_input_photo(char* filename)
{
    FILE* file = fopen(filename, "rb");
    struct ppm_data rv = read_header_data(file);
    int triplets = rv.width * rv.height;
    rv.rgb_data = (struct int_triplet*) malloc(triplets * sizeof(struct int_triplet));

    for(int i = 0; i < triplets; ++i)
    {
        unsigned char buffer[3];
        fread(buffer, 3, sizeof(char), file);
        rv.rgb_data[i].first = buffer[0];
        rv.rgb_data[i].second = buffer[1];
        rv.rgb_data[i].third = buffer[2];
    }

    fclose(file);

    return rv;
}

struct ycbcr_block_data convert_rgb_to_ycbcr(struct ppm_data* rgb_data)
{
    int blocks_per_row = rgb_data->width / 8;
    int length = rgb_data->width * rgb_data->height;

    struct ycbcr_block_data rv =
    {
        .width = rgb_data->width,
        .height = rgb_data->height,
        .blocks = (struct double_block*) malloc((rgb_data->width / 8) * (rgb_data->height / 8) * sizeof(struct double_block))
    };

    struct int_triplet* rgb = rgb_data->rgb_data;
    for(int i = 0; i < length; ++i, ++rgb)
    {
        int current_row = i / rgb_data->width;
        int current_column = i % rgb_data->width;
        int block_row = current_row / 8;
        int block_column = current_column / 8;
        int block_index = block_row * blocks_per_row + block_column;

        rv.blocks[block_index].first[current_row % 8][current_column % 8] = 0.299 * rgb->first + 0.587 * rgb->second + 0.114 * rgb->third;
        rv.blocks[block_index].second[current_row % 8][current_column % 8] = -0.1687 * rgb->first - 0.3313 * rgb->second + 0.5 * rgb->third + 128;
        rv.blocks[block_index].third[current_row % 8][current_column % 8] = 0.5 * rgb->first - 0.4186 * rgb->second - 0.0813 * rgb->third + 128;
    }

    return rv;
}

void shift_ycbcr(struct ycbcr_block_data* ycbcr, int value)
{
    int block_count = (ycbcr->width / 8) * (ycbcr->height / 8);
    for(int b = 0; b < block_count; ++b)
    {
        struct double_block* block = ycbcr->blocks + b;
        for(int u = 0; u < 8; ++u)
        {
            for(int v = 0; v < 8; ++v)
            {
                block->first[u][v] += value;
                block->second[u][v] += value;
                block->third[u][v] += value;
            }
        }
    }
}

void discrete_cosine_transform(struct ycbcr_block_data* ycbcr)
{
    int block_count = (ycbcr->width / 8) * (ycbcr->height / 8);
    for(int b = 0; b < block_count; ++b)
    {
        struct double_block* block = ycbcr->blocks + b;
        ippiDCT8x8Fwd_32f_C1I(block->first);
        ippiDCT8x8Fwd_32f_C1I(block->second);
        ippiDCT8x8Fwd_32f_C1I(block->third);
    }
}

void quantisation(struct ycbcr_block_data* ycbcr)
{
    const float k1_table[8][8] =
    {
        { 16, 11, 10, 16, 24, 40, 51, 61 },
        { 12, 12, 14, 19, 26, 58, 60, 55 },
        { 14, 13, 16, 24, 40, 57, 69, 56 },
        { 14, 17, 22, 29, 51, 87, 80, 62 },
        { 18, 22, 37, 56, 68, 109, 103, 77 },
        { 24, 35, 55, 64, 81, 104, 113, 92 },
        { 49, 64, 78, 87, 103, 121, 120, 101 },
        { 72, 92, 95, 98, 112, 100, 103, 99 }
    };

    const float k2_table[8][8] =
    {
        { 17, 18, 24, 47, 99, 99, 99, 99 },
        { 18, 21, 26, 66, 99, 99, 99, 99 },
        { 24, 26, 56, 99, 99, 99, 99, 99 },
        { 47, 66, 99, 99, 99, 99, 99, 99 },
        { 99, 99, 99, 99, 99, 99, 99, 99 },
        { 99, 99, 99, 99, 99, 99, 99, 99 },
        { 99, 99, 99, 99, 99, 99, 99, 99 },
        { 99, 99, 99, 99, 99, 99, 99, 99 }
    };

    int block_count = (ycbcr->width / 8) * (ycbcr->height / 8);
    for(int b = 0; b < block_count; ++b)
    {
        struct double_block* block = ycbcr->blocks + b;
        for(int u = 0; u < 8; ++u)
        {
            for(int v = 0; v < 8; ++v)
            {
                block->first[u][v] = round(block->first[u][v] / k1_table[u][v]);
                block->second[u][v] = round(block->second[u][v] / k2_table[u][v]);
                block->third[u][v] = round(block->third[u][v] / k2_table[u][v]);
            }
        }
    }
}

struct ycbcr_block_data encode(char* filename)
{
    struct ppm_data rgb = read_input_photo(filename);
    struct ycbcr_block_data ycbcr = convert_rgb_to_ycbcr(&rgb);
    shift_ycbcr(&ycbcr, -128);
    discrete_cosine_transform(&ycbcr);
    quantisation(&ycbcr);

    free_ppm_data(&rgb);

    return ycbcr;
}

void write_to_file(struct ycbcr_block_data* ycbcr, char* filename)
{
    FILE* file = fopen(filename, "w");
    fprintf(file, "%d %d\n\n", ycbcr->width, ycbcr->height);

    int block_count = (ycbcr->width / 8) * (ycbcr->height / 8);
    for(int b = 0; b < block_count; ++b)
    {
        struct double_block* block = ycbcr->blocks + b;
        for(int i = 0; i < 64; i++)
        {
            fprintf(file, "%d ", (int)block->first[zig_zag[i].row][zig_zag[i].coloumn]);
        }
    }
    fprintf(file, "\n\n");
    for(int b = 0; b < block_count; ++b)
    {
        struct double_block* block = ycbcr->blocks + b;
        for(int i = 0; i < 64; i++)
        {
            fprintf(file, "%d ", (int)block->second[zig_zag[i].row][zig_zag[i].coloumn]);
        }
    }
    fprintf(file, "\n\n");
    for(int b = 0; b < block_count; ++b)
    {
        struct double_block* block = ycbcr->blocks + b;
        for(int i = 0; i < 64; i++)
        {
            fprintf(file, "%d ", (int)block->third[zig_zag[i].row][zig_zag[i].coloumn]);
        }
    }
    fprintf(file, "\n");
}
